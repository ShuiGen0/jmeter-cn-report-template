## 将report-template目录替换apache-jmeter-x.x\bin\report-template目录即可
下载：https://gitee.com/smooth00/jmeter-cn-report-template/releases

## 可以写个bat或sh进行html报告批量生成

目前是UTF-8 - 无BOM 格式，这样可以同时兼容Windows和Linux下的中文显示
<br>如果生成的html报告显示中文乱码，自行将模板文件转存合适的编码格式（Windows下可以将所有包括content/pages下的.fmkr文件都另存为ANSI/ASCII格式）
<br>效果图：<br>
<img src="http://img-blog.csdnimg.cn/20190912154547723.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9zbW9vdGguYmxvZy5jc2RuLm5ldA==" align="center" />
<br>
5.x版本比4.x新增了自定义图表（还不知道怎么用）
